from django.db import models
from django.contrib import admin
from django.db.models import ForeignKey, OneToOneField


class BaseModel(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True


class RawIdFieldForAllMixin:
    raw_id_fields = []

    def __init__(self, model, admin_site, *args, **kwargs):
        self.raw_id_fields = self.setup_raw_id_fields(model)
        super().__init__(model, admin_site, *args, **kwargs)

    def setup_raw_id_fields(self, model):
        return list(
            f.name
            for f in model._meta.get_fields()
            if isinstance(f, ForeignKey) or isinstance(f, OneToOneField)
        )


class BaseAdmin(RawIdFieldForAllMixin, admin.ModelAdmin):
    readonly_fields = ["created", "updated"]
