import re
import traceback
from datetime import datetime, timedelta

import gspread
import jdatetime
import pandas as pd
import pytz
import telegram as telegram
from django.core.exceptions import ValidationError
from django.shortcuts import render
from oauth2client.service_account import ServiceAccountCredentials
from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView
from telegram import Update, InlineKeyboardMarkup, InlineKeyboardButton
from telegram.ext import Dispatcher, CommandHandler, CallbackContext, MessageHandler, Filters

from food_reminder.settings import TOKEN, MY_TELEGRAM_ID
from subscription.models import Subscriber
from utils.helpers import get_tomorrow_weekday_num, month_short_name_to_num, weekday_num_to_weekday_name


class UpdateHandler(APIView):
    def post(self, request):
        bot = telegram.Bot(TOKEN)
        update = Update.de_json(request.data, bot)
        dispatcher = Dispatcher(bot, None, workers=5)

        welcome_handler = CommandHandler("start", UpdateHandler.send_welcome_template)
        reservations_handler = CommandHandler("reservations", UpdateHandler.send_reservations)
        default_handler = MessageHandler(filters=Filters.text, callback=UpdateHandler.default_message_handler)

        dispatcher.add_handler(welcome_handler)
        dispatcher.add_handler(reservations_handler)
        dispatcher.add_handler(default_handler)

        dispatcher.process_update(update)

        return Response({"success": True}, status=status.HTTP_200_OK)

    # @classmethod
    # def get_day_reply_markup(cls):
    #     return InlineKeyboardMarkup([[InlineKeyboardButton(text='نمای هفته', callback_data='week_view')],
    #
    #                                  [InlineKeyboardButton(text='روز بعد', callback_data='sunday'),
    #                                   InlineKeyboardButton(text='روز قبل', callback_data='saturday')],
    #
    #                                  [InlineKeyboardButton(text='❌ جوجه‌کباب', callback_data='sunday')],
    #
    #                                  [InlineKeyboardButton(text='✅ پلو یونانی', callback_data='tuesday')]])


    @staticmethod
    def default_message_handler(update: Update, context: CallbackContext):
        message_text = update.message.text
        if Subscriber.objects.filter(email=message_text.lower()).exists():
            UpdateHandler.send_duplicate_email_error(update)
            return
        subscriber_query = Subscriber.objects.filter(telegram_id=update.message.from_user.id)
        try:
            if subscriber_query.exists():
                subscriber = subscriber_query.first()
                subscriber.email = message_text.lower()
                subscriber.save(update_fields=['updated', 'email'])
                UpdateHandler.send_email_updated_message(update)
            else:
                Subscriber.objects.create(telegram_id=update.message.from_user.id,
                                          email=message_text.lower())
                UpdateHandler.send_email_saved_message(update)
        except ValidationError:
            UpdateHandler.send_incorrect_error_message(update)

    @staticmethod
    def send_welcome_template(update: Update, context: CallbackContext):
        template_text = (f"سلام "
                         f"{update.message.from_user.first_name}! "
                         f"لطفاً ایمیل خود را وارد کنید:")

        update.message.reply_text(text=template_text)

    @staticmethod
    def send_incorrect_error_message(update):
        update.message.from_user.send_message("ایمیل واردشده معتبر نیست. لطفاً دوباره ایمیل خود را وارد کنید:")

    @staticmethod
    def send_email_updated_message(update):
        update.message.from_user.send_message("ایمیل شما با موفقیت به‌روزرسانی شد.")

    @staticmethod
    def send_email_saved_message(update):
        update.message.from_user.send_message("ایمیل شما با موفقیت ذخیره شد.")

    @staticmethod
    def send_reservations(update: Update, context: CallbackContext):
        try:
            SCOPES = ['https://spreadsheets.google.com/feeds', 'https://www.googleapis.com/auth/drive']
            creds = ServiceAccountCredentials.from_json_keyfile_name('credentials.json', SCOPES)

            client = gspread.authorize(creds)
            sheet = client.open('تعداد غذاهای تبدیل')

            tehran = pytz.timezone("Asia/Tehran")

            worksheet_names = sheet.worksheets()
            chosen_worksheet_name = None

            today = datetime.now(tz=tehran)
            j_today = jdatetime.datetime.fromgregorian(date=today).date()
            j_weekday = j_today.weekday()

            which_week = "جاری"
            if j_weekday == 6:
                j_today += jdatetime.timedelta(days=1)
                which_week = "آینده"
            j_today_year = j_today.year

            for worksheet_name in worksheet_names:
                if len(worksheet_name.title.split('-')) == 4:
                    start_day, start_month, end_day, end_month = worksheet_name.title.replace(" ", "").split('-')
                    start_day = int(start_day)
                    start_month = start_month[:3]
                    start_month_num = month_short_name_to_num(start_month)
                    end_day = int(end_day)
                    end_month = end_month[:3]
                    end_month_num = month_short_name_to_num(end_month)
                    if end_month_num < start_month_num:
                        year_diff = 1
                    else:
                        year_diff = 0

                    start_date = jdatetime.date(j_today_year, start_month_num, start_day)
                    end_date = jdatetime.date(j_today_year + year_diff, end_month_num, end_day)

                    if start_date <= j_today <= end_date:
                        chosen_worksheet_name = worksheet_name.title
                        break

            if chosen_worksheet_name is not None:
                sheet_instance = sheet.worksheet(chosen_worksheet_name)
                df: pd.DataFrame = pd.DataFrame.from_dict(sheet_instance.get_all_values()).iloc[:, :9]
                headers = df.iloc[0]
                df = df[1:]
                df.columns = headers
                df["Email Address"] = df["Email Address"].apply(lambda x: x.lower())

                subscriber = Subscriber.objects.filter(telegram_id=update.message.from_user.id).first()
                if subscriber is not None:
                    df_filter = df[df['Email Address'] == subscriber.email]
                    if df_filter.shape[0] == 1:
                        text_lines = [f'رزروهای شما برای هفته‌ی '
                                      f"{which_week} "
                                      f"({start_day} {jdatetime.datetime.j_months_fa[start_month_num - 1]} - {end_day} {jdatetime.datetime.j_months_fa[end_month_num - 1]}):\n"]
                        for i in range(6):
                            text_lines.append(f"{df_filter.columns[3 + i]}: {df_filter.iloc[:, i + 3].values[0]}")

                        text_lines.append("‌")
                        text = '\n'.join(text_lines)
                        update.message.reply_text(text=text)
                    elif df_filter.shape[0] == 0:
                        update.message.reply_text(text=f"شما برای هفته‌ی "
                                                       f"{which_week} "
                                                       f"غذایی رزرو نکرده‌اید.")
        except:
            update.message.reply_text(text="با عرض پوزش، خطایی رخ داده است. این خطا بررسی خواهد شد.")
            try:
                context.bot.send_message(MY_TELEGRAM_ID, traceback.format_exc())
            except:
                context.bot.send_message(MY_TELEGRAM_ID, "Error while sending the exception")
                with open("error_logs.txt", 'a') as f:
                    f.write(f"{traceback.format_exc()}\n\n")

    @classmethod
    def send_duplicate_email_error(cls, update):
        update.message.from_user.send_message("این ایمیل قبلاً ثبت شده است.")


class MarkThePaperView(APIView):
    def post(self, request):
        token = request.data.get('token')
        if token == TOKEN:
            subscribers = Subscriber.objects.filter(notify=True)
            for subscriber in subscribers:
                bot = telegram.Bot(TOKEN)
                text = 'اگه امروز غذا خوردی، یادت نره تو کاغذ علامت بزنی 😃'
                try:
                    bot.send_message(subscriber.telegram_id, text)
                except:
                    pass
            return Response({"success": True}, status=status.HTTP_200_OK)
        else:
            return Response({"success": False}, status=status.HTTP_400_BAD_REQUEST)


class RemindReservationView(APIView):
    def post(self, request):
        token = request.data.get('token')
        if token == TOKEN:
            subscribers = Subscriber.objects.filter(notify=True)
            for subscriber in subscribers:
                bot = telegram.Bot(TOKEN)
                text = 'اگه هفته دیگه غذا می‌خوای، یادت نره تا قبل فردا صبح ساعت ۱۰ غذا رو رزرو کنی!'
                try:
                    bot.send_message(subscriber.telegram_id, text)
                except telegram.error.Unauthorized:
                    subscriber.notify = False
                    subscriber.save(update_fields=['updated', 'notify'])
                except:
                    bot.send_message(MY_TELEGRAM_ID, f"{subscriber.id}\n{traceback.format_exc()}")
            return Response({"success": True}, status=status.HTTP_200_OK)
        else:
            return Response({"success": False}, status=status.HTTP_400_BAD_REQUEST)