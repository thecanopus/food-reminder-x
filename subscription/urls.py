from django.urls import path
from .views import UpdateHandler, MarkThePaperView, RemindReservationView

urlpatterns = [
    path('', UpdateHandler.as_view()),
    path('mark_the_paper/', MarkThePaperView.as_view()),
    path('remind_reserve/', RemindReservationView.as_view())
]
