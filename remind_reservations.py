# coding=utf-8

import os
import sys
import traceback
from datetime import datetime, timedelta
from time import sleep

import django
import jdatetime
import pytz
import telegram
import gspread
import pandas as pd
from oauth2client.service_account import ServiceAccountCredentials

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'food_reminder.settings')
django.setup()

from subscription.models import Subscriber
from food_reminder.local_settings import TOKEN
from food_reminder.settings import BASE_DIR, MY_TELEGRAM_ID
from utils.helpers import get_tomorrow_weekday_num, month_short_name_to_num, weekday_num_to_weekday_name


if __name__ == '__main__':
    try:
        SCOPES = ['https://spreadsheets.google.com/feeds', 'https://www.googleapis.com/auth/drive']
        bot = telegram.Bot(TOKEN)

        creds = ServiceAccountCredentials.from_json_keyfile_name(os.path.join(BASE_DIR, 'credentials.json'), SCOPES)

        try:
            client = gspread.authorize(creds)
            sheet = client.open('تعداد غذاهای تبدیل')
        except gspread.exceptions.APIError:
            error_message = "Cron-job failed because of gspread API call error."
            bot.send_message(MY_TELEGRAM_ID, error_message)
            raise Exception(error_message)

        tehran = pytz.timezone("Asia/Tehran")
        today_week_day = (datetime.now(tz=pytz.utc).astimezone(tehran).weekday() + 2) % 7
        tomorrow_week_day = get_tomorrow_weekday_num(today_week_day)

        worksheet_names = sheet.worksheets()
        chosen_worksheet_name = None

        tomorrow = datetime.now(tz=tehran) + timedelta(days=1)
        j_tomorrow = jdatetime.datetime.fromgregorian(date=tomorrow)
        j_tomorrow_month = j_tomorrow.month
        j_tomorrow_day = j_tomorrow.day

        for worksheet_name in worksheet_names:
            if len(worksheet_name.title.split('-')) == 4:
                start_day, start_month, end_day, end_month = worksheet_name.title.replace(" ", "").split('-')
                start_day = int(start_day)
                start_month = start_month[:3]
                start_month_num = month_short_name_to_num(start_month)
                end_day = int(end_day)
                end_month = end_month[:3]
                end_month_num = month_short_name_to_num(end_month)

                if (j_tomorrow_month == start_month_num and j_tomorrow_day >= start_day) or j_tomorrow_month > start_month_num:
                    if (j_tomorrow_month == end_month_num and j_tomorrow_day <= end_day) or j_tomorrow_month < end_month_num:
                        chosen_worksheet_name = worksheet_name.title

        if chosen_worksheet_name is None:
            subscribers = Subscriber.objects.filter(notify=True)
            for subscriber in subscribers:
                text = 'به نظر می‌رسد فرم غذا برای این هفته ساخته نشده است.'
                bot.send_message(subscriber.telegram_id, text)
        elif tomorrow_week_day != 6 and chosen_worksheet_name is not None:
            sheet_instance = sheet.worksheet(chosen_worksheet_name)
            df = pd.DataFrame.from_dict(sheet_instance.get_all_values()).iloc[:, :9]
            df: pd.DataFrame
            headers = df.iloc[0]
            df = df[1:]
            df.columns = headers
            df['Email Address'] = df['Email Address'].apply(lambda x: x.lower())

            subscribers = Subscriber.objects.filter(notify=True)
            for subscriber in subscribers:
                try:
                    df_filter = df[df['Email Address'] == subscriber.email]
                    if df_filter.shape[0] == 1:
                        if df_filter.columns[tomorrow_week_day + 3][:2] == weekday_num_to_weekday_name(tomorrow_week_day)[:2]:
                            reserved_food = df_filter.iloc[:, tomorrow_week_day + 3].values[0]

                            text = (f"انتخاب شما برای فردا، "
                                    f"{weekday_num_to_weekday_name(tomorrow_week_day)}: \n"
                                    f"{reserved_food}")
                            bot.send_message(subscriber.telegram_id, text)
                    elif df_filter.shape[0] == 0:
                        bot.send_message(subscriber.telegram_id, "شما فرم رزرو را برای این هفته ارسال نکرده‌اید.")
                except telegram.error.Unauthorized:
                    subscriber.notify = False
                    subscriber.save(update_fields=['updated', 'notify'])
                except:
                    bot.send_message(MY_TELEGRAM_ID, f"{subscriber.id}\n{traceback.format_exc()}")
    except:
        try:
            bot.send_message(MY_TELEGRAM_ID, traceback.format_exc())
        except:
            bot.send_message(MY_TELEGRAM_ID, "Error while sending the exception")
            with open("error_logs.txt", 'a') as f:
                f.write(f"{traceback.format_exc()}\n\n")
